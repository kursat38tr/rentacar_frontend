import { symbolName } from 'typescript';
import '../support/commands';
import Chance from 'chance';

describe('User Login' , () =>{
  it('user login', ()=>{
    const username = 'admin';
    const password = 'oZys5uxWxr';

    cy.visit('http://localhost:4200/login')
    cy.contains('Please sign in')

    cy.get('input[name=username]').type(username);
    cy.get('input[name=password]').type(password);
    cy.get('button[type=submit]').click();

    cy.contains('User Management Portal');
  })
  // it('user failed to login', ()=>{
  //   const username = 'admin';
  //   const password = 'notagoodpassword';
  //
  //   cy.visit('http://localhost:4200/login')
  //   cy.contains('Please sign in')
  //
  //
  //   cy.get('input[name=username]').type(username);
  //   cy.get('input[name=password]').type(password);
  //   cy.get('button[type=submit]').click();
  //
  //   cy.contains('.notifier__notification-message', 'USERNAME / PASSWORD INCORRECT. PLEASE TRY AGAIN');
  // })
  // it('user cannot access register page when logged in ',  ()=> {
  //   cy.login();
  //   cy.visit('http://localhost:4200/register')
  //   cy.wait(2000)
  //   cy.url('http://localhost:4200/user/management')
  // });
  // it('admin can create user', ()=>{
  //   cy.login();
  //   cy.get("#userAddButton").click();
  //   cy.wait(2000)
  //   let chance = new Chance();
  //   const firstname = chance.word({ length: 6 });
  //   const lastname = chance.word({ length: 6 });
  //   const username = chance.word({ length: 6 });
  //   const email = chance.email();
  //   const active = 1;
  //   const notLocked = 1;
  //
  //
  //   cy.get('input[id=createNewUserFirstname]').type(firstname);
  //   cy.get('input[id=createNewUserLastname]').type(lastname);
  //   cy.get('input[id=createNewUserUsername]').type(username);
  //   cy.get('input[id=createNewUserEmail]').type(email);
  //
  //   cy.wait(2000)
  //   cy.get('button[name=createNewUserButton]').click();
  //
  //   cy.contains('.notifier__notification-message', 'added successfully');
  // })
});


