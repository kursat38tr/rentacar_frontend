import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { UserComponent } from './components/user/user.component';
// import { ProductListComponent } from './components/product-list/product-list.component';
// import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { AuthenticationGuard } from './guard/authentication.guard';
import { HomeComponent } from './components/home/home.component';
import { CarService } from './services/car.service';
import { CarComponent } from './components/car/car.component';
import { CarDetailsComponent } from './components/car-details/car-details.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },

  {
    path: 'user/management',
    component: UserComponent,
    canActivate: [AuthenticationGuard],
  },

  // { path: 'cars/:id', component: CarService },
  { path: 'cars/:id', component: CarDetailsComponent },
  { path: 'search/:keyword', component: CarComponent },
  { path: 'cars', component: CarComponent },
  {path: 'category/:id' , component: CarComponent},
  {path: 'category' , component: CarComponent},
  { path: '', redirectTo: '/cars', pathMatch: 'full' },
  { path: '**', redirectTo: '/cars', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
