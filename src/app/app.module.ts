import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';
// import { ProductListComponent } from 'a./compoanensaats/produàaact-list/product-list.component';
import { ProductService } from './services/product.service';

import { Routes, RouterModule } from '@angular/router';
import { ProductCategoryMenuComponent } from './components/product-category-menu/product-category-menu.component';
import { SearchComponent } from './components/search/search.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CartStatusComponent } from './components/cart-status/cart-status.component';
import { AuthenticationService } from './services/authentication.service';
import { UserService } from './services/user.service';
import { AuthInterceptor } from './interceptor/auth.interceptor';
import { AuthenticationGuard } from './guard/authentication.guard';
import { NotifierModule } from 'angular-notifier';
import { NotificationService } from './services/notification.service';
import { LoginComponent } from './components/login/login.component';
import { AppRoutingModule } from './app-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RegisterComponent } from './components/register/register.component';
import { CommonModule } from '@angular/common';
import { UserComponent } from './components/user/user.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { CarComponent } from './components/car/car.component';
import { CarService } from './services/car.service';
import { CarCategoryComponent } from './components/car-category/car-category.component';
import { CarAdminComponent } from './components/car-admin/car-admin.component';
import { CarDetailsComponent } from './components/car-details/car-details.component';

const routes: Routes = [

];

@NgModule({
  declarations: [
    AppComponent,
    // ProductListaaCoamponent,
    ProductCategoryMenuComponent,
    SearchComponent,
    ProductDetailsComponent,
    CartStatusComponent,
    LoginComponent,
    RegisterComponent,
    UserComponent,
    NavBarComponent,
    CarComponent,
    CarCategoryComponent,
    CarAdminComponent,
    CarDetailsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes, {relativeLinkResolution: 'legacy'}),
    NotifierModule,
    FormsModule,
    NgbModule,
    AppRoutingModule,
    CommonModule,
    ReactiveFormsModule,
  ],
  providers: [
    ProductService,
    AuthenticationService,
    UserService,
    NotificationService,
    AuthenticationGuard,
    CarService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
