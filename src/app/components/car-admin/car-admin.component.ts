import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { NotificationType } from 'src/app/enum/notification-type.enum';
import { CarCategory } from 'src/app/model/car-category';
import { CarService } from 'src/app/services/car.service';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-car-admin',
  templateUrl: './car-admin.component.html',
  styleUrls: ['./car-admin.component.css']
})
export class CarAdminComponent implements OnInit {

  public cars: CarCategory[];
  private subscriptions: Subscription[] = [];

  constructor(private carCategoryService: CarService, private notificationService: NotificationService) { }

  ngOnInit(): void {
  }

  

  

}
