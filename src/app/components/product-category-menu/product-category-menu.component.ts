import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ProductCategory } from 'src/app/common/product-category';
import { NotificationType } from 'src/app/enum/notification-type.enum';
import { CarCategory } from 'src/app/model/car-category';
import { CarService } from 'src/app/services/car.service';
import { NotificationService } from 'src/app/services/notification.service';
import { ProductService } from 'src/app/services/product.service';
import { CarCategoryComponent } from '../car-category/car-category.component';

@Component({
  selector: 'app-product-category-menu',
  templateUrl: './product-category-menu.component.html',
  styleUrls: ['./product-category-menu.component.css'],
})
export class ProductCategoryMenuComponent implements OnInit {
  public carCategory: CarCategory[];
  private subscriptions: Subscription[] = [];

  
  constructor(private carCartegoryService: CarService, private notificationService: NotificationService) {}

  ngOnInit(): void {
     this.listProductCategories(true);
  }

  // public listProductCategories() {
  //   this.carCartegoryService.getOnlyCarsCategory().subscribe((data) => {
  //     console.log('Car Categories=' + JSON.stringify(data));
  //     this.carCategory = data;
  //   });
  // }

  public listProductCategories(showNotification: boolean): void {
    this.subscriptions.push(
      this.carCartegoryService.getCarsCategory().subscribe(
        (data: CarCategory[]) => {
          this.carCategory = data as CarCategory[];
          console.log(this.carCategory);
          if (showNotification) {
            this.sendNotification(
              NotificationType.SUCCESS,
              ` Cars Category loaded successfully.`
            );
          }
        },
        (errorResponse: HttpErrorResponse) => {
          this.sendNotification(
            NotificationType.ERROR,
            errorResponse.error.message
          );
        }
      )
    );
  }

  private sendNotification(
    notificationType: NotificationType,
    message: string
  ): void {
    if (message) {
      this.notificationService.notify(notificationType, message);
    } else {
      this.notificationService.notify(
        notificationType,
        'An error occurred. Please try again.'
      );
    }
  }
}
