import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Car } from 'src/app/model/car';
import { CarService } from 'src/app/services/car.service';

@Component({
  selector: 'app-car-details',
  templateUrl: './car-details.component.html',
  styleUrls: ['./car-details.component.css']
})
export class CarDetailsComponent implements OnInit {

  public car: Car = new Car();

  constructor(
    private carService: CarService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe(() => {
      this.handleCarDetails();
    });
  }
  public handleCarDetails() {
    // get id id param string, convert to number

    const theCarId: number = +this.route.snapshot.paramMap.get('id');

    this.carService.getCar(theCarId).subscribe(
      data=> {
        this.car = data;
      }
    )
   }
}
